import React from 'react';
import { graphql } from 'gatsby';
// import SEO from '../components/SEO';
import Layout from '../components/Layout';
// import Call from '../components/Call';

const Contact = ({ data }) => {
  const { title } = data.markdownRemark.frontmatter;
  const { html } = data.markdownRemark;
  const maphtml = "<iframe width='100%' height='100%' id='mapcanvas' src='https://maps.google.com/maps?q=30%20Wall%20St,%20new%20york&amp;t=&amp;z=10&amp;ie=UTF8&amp;iwloc=&amp;output=embed' frameborder='0' scrolling='no' marginheight='0' marginwidth='0'><div class=\"zxos8_gm\"><a href=\"https://www.downloadyou.tube/\">Vimeo and Youtube can be downloaded from this website for free</a></div><div style='overflow:hidden;'><div id='gmap_canvas' style='height:100%;width:100%;'></div></div><div><small>Powered by <a href=\"https://www.embedgooglemap.co.uk\">Embed Google Map</a></small></div></iframe>";
  return (
    <Layout bodyClass="page-default-single">
      <div className="intro">
        <div className="container">
          <div className="row justify-content-start">

            <div className="col-12">
              <h1>{title}</h1>

            </div>
            <div className="col-12 col-md-6">
              {/* <Call showButton={false} /> */}
              <p>Get in contact via phone or email address below. Alternatively, send us a message using the form below.</p>
              <div className="contacttext">
                <img alt="nyc" className="img-fluid icons" src="/images/icons/email.png" />
                <p className="officetext"><a href="mailto:hello@socode.us​">hello@socode.us</a></p>
              </div>
              <div className="contacttext">
                <img alt="nyc" className="img-fluid icons" src="/images/icons/phone.png" />
                <p className="officetext">+1 833 243 8753</p>
              </div>


              {/* <div className="content mt-4" dangerouslySetInnerHTML={{ __html: html }} /> */}
                <form action="https://getform.io/f/d4afc34b-ba32-466d-ba6b-edd3780fb203" method="POST" netlify-honeypot="bot-field" data-netlify="true" name="contact">

                    <div class="form-group">
                      <label for="inputName">Name</label>
                      <input name="name" type="text" class="form-control input-box rm-border" id="inputName" placeholder="Email" />
                    </div>
                    <div class="form-group">
                      <label for="company">Company</label>
                      <input name="company" type="text" class="form-control input-box rm-border" id="company" placeholder="Company" />
                    </div>
                    <div class="form-group">
                      <label for="email">Email</label>
                      <input name="email" type="email" class="form-control input-box rm-border" id="email" placeholder="Email" />
                    </div>
                    <div class="form-group">
                      <label for="phone">Phone</label>
                      <input name="phone" placeholder="Phone" type="text" class="form-control input-box rm-border" id="phone" class="input-box form-control bfh-phone" data-format="+1 (ddd) ddd-dddd" />
                    </div>
                    <div class="form-group">
                      <label class="leftlabel" for="enquiry">Enquiry</label>
                      <textarea name="enquiry" type="textarea" class="form-control input-box rm-border" id="enquiry" placeholder="" />
                    </div>
                  <button type="submit" class="btn btn-primary">Send</button>
                </form>
                <input type="hidden" name="form-name" value="contact" />
              </div>

            <div className="col-12 col-md-6 mapp" >
              <img alt="nyc" className="img-fluid icons office" src="/images/icons/office.png" />

              <p className="officetext">30 Wall St, 8th Floor, New York, 10005</p>
              <div style={{minWidth:'500px',minHeight:'700px', display:'block'}} dangerouslySetInnerHTML={{ __html: maphtml }} />
              </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export const query = graphql`
  query($id: String!) {
    markdownRemark(id: { eq: $id }) {
      frontmatter {
        title
        path
      }
      fields {
        slug
      }
      html
    }
  }
`;

export default Contact;
