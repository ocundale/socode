import React from 'react';
import { Link, graphql } from 'gatsby';
import SEO from '../components/SEO';
import Layout from '../components/Layout';

const Clients = props => {
  const { intro } = props.data;

  return (
    <Layout bodyClass="page-services">
      <SEO title="Clients" />

      <div className="intro">
        <div className="container">
          <div className="row justify-content-start">
            <div className="col-12">
              <h1>Clients</h1>
              <hr/>
            </div>
            <div className="col-md-12 col-lg-8 order-2 order-md-1">
              <h2>Our Services</h2>
              <div class="service">
                <img alt="service" className="img-fluid icons" src="/images/icons/perm.png" />
                <h3>FTE / Permanent</h3>
                <p>SoCode provide our clients with FTE / Permanent Talent for some of the world’s leading FinTech companies at Scale.</p>
              </div>
              <div class="service">
                <img alt="service" className="img-fluid icons" src="/images/icons/contract.png" />
                <h3>Contract</h3>
                <p>Got a need for speed? SoCode’s Contract team can supply Talent at scale, taking on the  complexity, risk and responsibility of engaging with contract workers within the US markets.</p>
              </div>
              <div class="service">
                <img alt="service" className="img-fluid icons" src="/images/icons/search.png" />
                <h3>Executive Search</h3>
                <p>Our most Robust and Rigorous solution -  designed for organisations who need to recruit Senior Level,  Business Critical or Niche Roles</p>
              </div>
              <div class="service">
                <img alt="service" className="img-fluid icons" src="/images/icons/rpo.png" />
                <h3>RPO & MSP</h3>
                <p>Need a bit more help with recruitment? SoCode are experienced in sending our Consultants on-site and have delivered several RPO Services to multiple NASDAQ / FTSE organisations, working alongside and giving Internal teams the Support they need. Enquire Here (Link)</p>
              </div>
            </div>
            <div className="hidden-md-down col-lg-4 order-2 order-md-2" style={{margin:'auto'}}>
              <img alt="nyc" className="img-fluid imgright" src="/images/illustrations/office.png" />
            </div>
          </div>
        </div>
      </div>
      <div className="strip centre-all strip-grey">
          <div className="container pt-6 pb-6 pb-md-10">

              <h2>Specialisms</h2>
              <hr />

            <div className="row justify-content-start">

                <div className="col-12 col-md-4 mb-1">
                  <div className="service service-summary">
                    <div className="service-content">
                      <h3 className="service-title">
                      Software Engineering, DevOps & QA
                      </h3>
                      {/* <p>{node.excerpt}</p> */}
                    </div>
                  </div>
                </div>

                <div className="col-12 col-md-4 mb-1">
                  <div className="service service-summary">
                    <div className="service-content">
                      <h3 className="service-title">
                      Machine Learning, Artificial Intelligence & Big Data
                      </h3>
                      {/* <p>{node.excerpt}</p> */}
                    </div>
                  </div>
                </div>

                <div className="col-12 col-md-4 mb-1">
                  <div className="service service-summary">
                    <div className="service-content">
                      <h3 className="service-title">
                      Project Management & Business Analysis
                      </h3>
                      {/* <p>{node.excerpt}</p> */}
                    </div>
                  </div>
                </div>
                </div>

              <div className="row justify-content-start">

                <div className="col-12 col-md-4 mb-1">
                  <div className="service service-summary">
                    <div className="service-content">
                      <h3 className="service-title">
                      Product Management
                      </h3>
                      {/* <p>{node.excerpt}</p> */}
                    </div>
                  </div>
                </div>

                <div className="col-12 col-md-4 mb-1">
                  <div className="service service-summary">
                    <div className="service-content">
                      <h3 className="service-title">
                      Infrastructure & Support
                      </h3>
                      {/* <p>{node.excerpt}</p> */}
                    </div>
                  </div>
                </div>

                <div className="col-12 col-md-4 mb-1">
                  <div className="service service-summary">
                    <div className="service-content">
                      <h3 className="service-title">
                      Technology Leadership
                      </h3>
                      {/* <p>{node.excerpt}</p> */}
                    </div>
                  </div>
                </div>

            </div>
            {/* <div className="row justify-content-center">
              <div className="col-auto">
                <Link className="button button-primary" to="/services/">View All Services</Link>
              </div>
            </div> */}
          </div>
        </div>


      {/* <div className="container">
        <div className="row">

          <div className="col-12 col-md-6 mb-2">
            <div className="team team-summary team-summary-large">
                <div className="team-image">
                  <img alt={`photo of`} className="img-fluid mb-2" src="/images/team/joseph-gonzalez-399972-unsplash.jpg" />
                </div>
              <div className="team-meta">
                <h2 className="team-name">JP Morgan</h2>
                <p className="team-description">Banking</p>
                  <a target="_blank" href="">Website</a>
              </div>
              <div className="team-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Bibendum arcu…</p>
              </div>
            </div>
          </div>
          <div className="col-12 col-md-6 mb-2">
            <div className="team team-summary team-summary-large">
                <div className="team-image">
                  <img alt={`photo of`} className="img-fluid mb-2" src="/images/team/joseph-gonzalez-399972-unsplash.jpg" />
                </div>
              <div className="team-meta">
                <h2 className="team-name">JP Morgan</h2>
                <p className="team-description">Banking</p>
                  <a target="_blank" href="">Website</a>
              </div>
              <div className="team-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Bibendum arcu…</p>
              </div>
            </div>
          </div>

        </div>
      </div> */}


    </Layout>
  );
};

export const query = graphql`
  query ClientsQuery {
    clients: allMarkdownRemark(
      filter: { fileAbsolutePath: { regex: "/clients\/.*/" } }
      sort: { fields: [frontmatter___date], order: DESC }
    ) {
      edges {
        node {
          id
          excerpt
          fields {
            slug
          }
          frontmatter {
            title
          }
        }
      }
    }
    intro: markdownRemark(fileAbsolutePath: {regex: "/(clients.md)/"}) {
      html
      frontmatter {
        title
        image
        intro_image
        intro_image_absolute
        intro_image_hide_on_mobile
      }
    }
  }
`;

export default Clients;
