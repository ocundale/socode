import React from 'react';
import { graphql, Link } from 'gatsby';
import Helmet from 'react-helmet';
import SEO from '../components/SEO';
import Layout from '../components/Layout';
import Call from '../components/Call';

const Home = props => {
  const intro = props.data.intro;
  const site = props.data.site.siteMetadata;
  const clients = props.data.clients.edges;
  const features = props.data.features.edges;
  const introImageClasses = `intro-image ${intro.frontmatter.intro_image_absolute && 'intro-image-absolute'} ${intro.frontmatter.intro_image_hide_on_mobile && 'intro-image-hide-mobile'}`;

  return (
    <Layout bodyClass="page-home">
      <SEO title={site.title} />
      <Helmet>
        <meta
          name="description"
          content="Small Business Theme. Multiple content types using Markdown and JSON sources. Responsive design and SCSS. This is a beautiful and artfully designed starting theme."
        />
      </Helmet>

      {/* <div className="intro">
        <div className="container">
          <div className="row justify-content-start">
          {intro.frontmatter.intro_image && (
              <div className="col-12 col-md-5 col-lg-6 order-1 order-md-2 position-relative">
                <img alt={intro.frontmatter.title} className={introImageClasses} src={intro.frontmatter.intro_image} />
              </div>
            )}
            <div className="col-12 col-md-7 col-lg-6 order-2 order-md-1">
              <div dangerouslySetInnerHTML={{ __html: intro.html }} />
               <Call showButton />
            </div>

          </div>
        </div>
      </div> */}

      <div className="intro image-bg">
        <div className="container">
          <div className="row justify-content-start">
              <img className="centreimg" src="/images/logo/logopng.png" />

              <h1 className="centretext">US Fintech Recruitment</h1>
              {/* <Call showButton /> */}
          </div>
        </div>
      </div>


      <div className="strip centre-all strip-grey">
          <div className="container pt-6 pb-6 pb-md-10">

              <h2>Specialisms</h2>
            <hr />
            <div className="row justify-content-start">

                <div className="col-12 col-md-4 mb-1">
                  <div className="service service-summary">
                    <div className="service-content">
                      <h3 className="service-title">
                      Software Engineering, DevOps & QA
                      </h3>
                      {/* <p>{node.excerpt}</p> */}
                    </div>
                  </div>
                </div>

                <div className="col-12 col-md-4 mb-1">
                  <div className="service service-summary">
                    <div className="service-content">
                      <h3 className="service-title">
                      Machine Learning, Artificial Intelligence & Big Data
                      </h3>
                      {/* <p>{node.excerpt}</p> */}
                    </div>
                  </div>
                </div>

                <div className="col-12 col-md-4 mb-1">
                  <div className="service service-summary">
                    <div className="service-content">
                      <h3 className="service-title">
                      Project Management & Business Analysis
                      </h3>
                      {/* <p>{node.excerpt}</p> */}
                    </div>
                  </div>
                </div>
                </div>

              <div className="row justify-content-start">

                <div className="col-12 col-md-4 mb-1">
                  <div className="service service-summary">
                    <div className="service-content">
                      <h3 className="service-title">
                      Product Management
                      </h3>
                      {/* <p>{node.excerpt}</p> */}
                    </div>
                  </div>
                </div>

                <div className="col-12 col-md-4 mb-1">
                  <div className="service service-summary">
                    <div className="service-content">
                      <h3 className="service-title">
                      Infrastructure & Support
                      </h3>
                      {/* <p>{node.excerpt}</p> */}
                    </div>
                  </div>
                </div>

                <div className="col-12 col-md-4 mb-1">
                  <div className="service service-summary">
                    <div className="service-content">
                      <h3 className="service-title">
                      Technology Leadership
                      </h3>
                      {/* <p>{node.excerpt}</p> */}
                    </div>
                  </div>
                </div>

            </div>
            {/* <div className="row justify-content-center">
              <div className="col-auto">
                <Link className="button button-primary" to="/services/">View All Services</Link>
              </div>
            </div> */}
          </div>
        </div>


      <div className="strip centre-all">
        <div className="container pt-6 pb-6 pb-md-10">
          <div className="row justify-content-center">
            <div className="hometext" dangerouslySetInnerHTML={{ __html: intro.html }} />
          </div>
          <div className="row justify-content-center">
            <div className="col-auto">
              <Link className="button button-primary" to="/contact/">Contact us</Link>
            </div>
          </div>
        </div>
      </div>

      {features.length > 0 && (
        <div className="strip strip-grey">
          <div className="container pt-6 pb-6 pt-md-10 pb-md-10">
            <div className="row justify-content-center">
              {features.map(({ node }) => (
                <div key={node.id} className="col-12 col-md-6 col-lg-4 mb-2">
                  <div className="feature">
                    {node.image && (
                      <div className="feature-image">
                        <img src={node.image} />
                      </div>
                    )}
                    <h2 className="feature-title">{node.title}</h2>
                    <div className="feature-content">{node.description}</div>
                    <div className="feature-content small">{node.smalltext}</div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      )}








    </Layout>
  );
};

export const query = graphql`
  query {
    clients: allMarkdownRemark(
      filter: { fileAbsolutePath: { regex: "/clients\/.*/" } }
      sort: { fields: [frontmatter___weight], order: ASC }
      limit: 6
    ) {
      edges {
        node {
          id
          frontmatter {
            title
            date(formatString: "DD MMMM YYYY")
          }
          fields {
            slug
          }
          excerpt
        }
      }
    }
    intro: markdownRemark(
      fileAbsolutePath: {regex: "/content/index.md/"}
    ) {
        html
        frontmatter {
          image
          intro_image
          intro_image_absolute
          intro_image_hide_on_mobile
          title
        }
    }
    features: allFeaturesJson {
      edges {
        node {
          id
          title
          description
          image
          smalltext
        }
      }
    }
    site {
      siteMetadata {
        title
      }
    }
  }
`;

export default Home;
