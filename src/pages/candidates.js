import React from 'react';
import { Link, graphql } from 'gatsby';
import SEO from '../components/SEO';
import Layout from '../components/Layout';
import Call from '../components/Call';

const Team = props => {
  const team = props.data.team.edges;

  return (
    <Layout bodyClass="page-teams">
      <SEO title="Team" />

      <div className="intro">
        <div className="container">
          <div className="row justify-content-start">
            <div className="col-12 col-md-12 col-lg-12 order-2 order-md-1">
              <h1>Candidates</h1>
              <p>Please send your CV through to us at <a href="mailto:admin@socode.us">jobs@socode.us</a></p>
            </div>
          </div>
        </div>
      </div>

      <div className="container">
        <div className="row">
          {/* <div className="col-12 col-md-6 mb-2">
            <div className="team team-summary team-summary-large">
                <div className="team-image">
                  <img alt={`photo of`} className="img-fluid mb-2" src="/images/team/sage-kirk-485982-unsplash.jpg" />
                </div>
              <div className="team-meta">
                <h2 className="team-name">Someone Name</h2>
                <p className="team-description">Developer</p>
                  <a target="_blank" href="">LinkedIn</a>
              </div>
              <div className="team-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Bibendum arcu…</p>
              </div>
            </div>
          </div>
          <div className="col-12 col-md-6 mb-2">
            <div className="team team-summary team-summary-large">
                <div className="team-image">
                  <img alt={`photo of`} className="img-fluid mb-2" src="/images/team/sage-kirk-485982-unsplash.jpg" />
                </div>
              <div className="team-meta">
                <h2 className="team-name">Someone Name</h2>
                <p className="team-description">Developer</p>
                  <a target="_blank" href="">LinkedIn</a>
              </div>
              <div className="team-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Bibendum arcu…</p>
              </div>
            </div>
          </div> */}
          {/* {team.filter(edge => (edge.node.frontmatter.promoted)).map(({ node }) => (
            <div key={node.id} className="col-12 col-md-6 mb-2">
              <div className="team team-summary team-summary-large">
                {node.frontmatter.image && (
                  <div className="team-image">
                    <img alt={`photo of ${node.frontmatter.title}`} className="img-fluid mb-2" src={node.frontmatter.image} />
                  </div>
                )}
                <div className="team-meta">
                  <h2 className="team-name">{node.frontmatter.title}</h2>
                  <p className="team-description">{node.frontmatter.jobtitle}</p>
                  {node.frontmatter.linkedin && (
                    <a target="_blank" href="{{ .Params.Linkedinurl }}">LinkedIn</a>
                  )}
                </div>
                <div className="team-content">
                  <p>{node.excerpt}</p>
                </div>
              </div>
            </div>
          ))} */}
        </div>
      </div>

    </Layout>
  );
};

export const query = graphql`
  query TeamQuery {
    team: allMarkdownRemark(
      filter: { fileAbsolutePath: { regex: "/team\/.*/" } }
      sort: { fields: [frontmatter___date], order: DESC }
    ) {
      edges {
        node {
          id
          excerpt
          fields {
            slug
          }
          frontmatter {
            title
            promoted
            image
            jobtitle
            linkedinurl
          }
        }
      }
    }
  }
`;

export default Team;
