---
title: 'Clients'
# intro_image: "/images/illustrations/reading.svg"
intro_image_absolute: true
intro_image_hide_on_mobile: false
---

## Our Services

### FTE / Permanent
SoCode provide our clients with FTE / Permanent Talent for some of the world’s leading FinTech companies at Scale.

### Contract
Got a need for speed? SoCode’s Contract team can supply Talent at scale, taking on the  complexity, risk and responsibility of engaging with contract workers within the US markets.

### Executive Search
Our most Robust and Rigorous solution -  designed for organisations who need to recruit Senior Level,  Business Critical or Niche Roles

### RPO & MSP
Need a bit more help with recruitment? SoCode are experienced in sending our Consultants on-site and have delivered several RPO Services to multiple NASDAQ / FTSE organisations, working alongside and giving Internal teams the Support they need. Enquire Here (Link)

