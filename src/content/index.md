---
title: 'Home'
intro_image: "/images/illustrations/nyc.png"
intro_image_absolute: true
intro_image_hide_on_mobile: true
---

## What we do

SoCode connects the world’s leading Banking and Payments companies with the Talent that builds the world’s financial Infrastructure.

Operating since 2016 - our team has been responsible for providing talent into some of the fastest growing Challenger, Investment Banks and Payment Gateways globally.